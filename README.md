# Gojek-ParkingLot

#### Preparation
make sure you've been installed Node.js on your computer before running this application.

run `bin/setup` to install all dependencies first


#### Run The Application
then run `bin/parking_lot` to run the application. Type `interactive` to starting with Interactive mode or Type `file` to run the application with .txt file

#### File mode
After type `file` make sure you type the correct path for .txt file 

### Testing
Type  'test' after run `bin/parking_lot`.
this will running file_input.txt to test the application