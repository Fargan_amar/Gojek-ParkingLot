const assert = require('chai').assert,
    fs = require('fs'),
    Parking = require('../src/model/parking');

var commands = [],
    totalParkings,
    parkingLot = new Parking();

// test specs for unit testing the methods in Parking Lot class
describe('Test for reading input test data', function () {
    it('reading input.txt', function (done) {
        fs.readFile('./functional_spec/fixtures/file_input.txt', 'utf-8', function (err, data) {
            if (err) {
                throw 'Unable to read input test file, message : ' + err;
            }
            commands = JSON.parse(JSON.stringify(data)).split('\n');
            done();
        });
    });

    it('checking commands', function (done) {
        assert.equal(commands[0].split(' ')[0], 'create_parking_lot');
        assert.equal(commands[1].split(' ')[0], 'park');
        assert.equal(commands[7].split(' ')[0], 'leave');
        assert.equal(commands[8], 'status');
        done();
    });
});

// unit tests for functions in ParkingLot class
describe('Testing Functions in ParkingLot class', function () {

    it('Creating a Parking lot', function (done) {
        totalParkings = parkingLot.createLot(commands[0]);
        assert.equal(totalParkings, 'Created a parking lot with 6 slots');
        done();
    });

    it('Allocating Parking to User 1', function (done) {
        let result = parkingLot.parkTheCar(commands[1]);
        assert.equal(result, 'Allocated slot number: 1', 'these numbers are equal');
        done();
    });

    it('Allocating Parking to User 2', function (done) {
        let result = parkingLot.parkTheCar(commands[2]);
        assert.equal(result, 'Allocated slot number: 2');
        done();
    });

    it('Allocating Parking to User 3', function (done) {
        let result = parkingLot.parkTheCar(commands[3]);
        assert.equal(result, 'Allocated slot number: 3');
        done();
    });

    it('Allocating Parking to User 4', function (done) {
        let result = parkingLot.parkTheCar(commands[4]);
        assert.equal(result, 'Allocated slot number: 4');
        done();
    });

    it('Allocating Parking to User 5', function (done) {
        let result = parkingLot.parkTheCar(commands[5]);
        assert.equal(result, 'Allocated slot number: 5');
        done();
    });

    it('Allocating Parking to User 6', function (done) {
        let result = parkingLot.parkTheCar(commands[6]);
        assert.equal(result, 'Allocated slot number: 6');
        done();
    });

    it('Leaving from slot 4', function (done) {
        let result = parkingLot.leaveCar(commands[7]);
        assert.equal(result, 'Slot number 4 is free');
        done();
    });

    it('Checking status', function (done) {
        let result = parkingLot.getStatusOfParking();
        assert.equal(result.length, 155);
        done();
    });

    it('Allocating Parking to User 7. Should Reallocate the nearest empty postion 4', function (done) {
        let result = parkingLot.parkTheCar(commands[9]);
        assert.equal(result, 'Allocated slot number: 4');
        assert.notEqual(result, 'Allocated slot number: 7');
        done();
    });

    it('Allocating Parking to User 8. Should indicate Parking is full.', function (done) {
        try {
            var result = parkingLot.parkTheCar(commands[10]);
        } catch (err) {
            assert.notEqual(result, 'Allocated slot number: 8');
        }
        done();
    });

    it('Registration no. for cars with white color', function (done) {
        let result = parkingLot.findCarBySimilarColor(commands[11]);
        result = result.split(', ');
        assert.equal(result[0], 'KA-01-HH-1234');
        assert.equal(result[1], 'KA-01-HH-9999');
        assert.equal(result[2], 'KA-01-P-333');
        done();
    });

    it('Slot no. for cars with white color', function (done) {
        let result = parkingLot.calculateOfSimilarColor(commands[12]);
        result = result.split(',').map(Number);
        assert.equal(result[0], 1);
        assert.equal(result[1], 2);
        assert.equal(result[2], 4);
        done();
    });

    it('Slot no. for registration no. KA-01-HH-3141', function (done) {
        let result = parkingLot.findByPlate(commands[13]);
        assert.equal(result, 6);
        done();
    });

    it('Slot no. for registration no. MH-04-AY-1111', function (done) {
        let result = parkingLot.findByPlate(commands[14]);
        assert.equal(result, 'Not found');
        done();
    });

});
