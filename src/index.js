const fs    = require('fs')
const chalk = require('chalk')
const readLine  = require('readline')
const sh    = require('shelljs')
const Parking   = require('./model/parking')
const parkingLot    = new Parking()

require('events').EventEmitter.defaultMaxListeners = 0;

let   interactiveType   = false

const ui = readLine.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

main()

function main () {
    ui.question(`Type 'interactive' or 'file' or 'test' : `, (data) => {
        if (data.toLowerCase() === 'interactive' ) {
            interactiveType = true
            createInteractiveModeConsole()
        } else if(data.toLowerCase() === 'file'){
            fileInteractiveMode()        
        } else if (data.toLowerCase() === 'test') {
            sh.exec('npm test')
            process.exit(0)
        }
    })
}



async function createInteractiveModeConsole () {
    if (interactiveType) {
        ui.question('Input: ',  (data) => {
             interactiveCommand(data)
        })
    }
}

async function fileInteractiveMode () {
    ui.question(`Type path of the .txt file example:'functional_spec/fixtures/file_input.txt' : ` , (data) => {
        interactiveType = false

        fs.readFile(data, 'utf-8', (error, dataFile) => {
            if (error) {
                console.log(chalk.red.bold(console.log('Error in reading file')))
                main()
            } else {
                const arrData   = dataFile.split('\n')
                arrData.forEach((row, index) => {
                     interactiveCommand(arrData[index])
                })
                process.exit(0)
            }
        })
        
    })

}

let interactiveCommand = async (input) => {
    const   usrCmd  = input.split(' ')[0]
    // let   totalSlots, parkingSlotNumber, parkingSlotNumbers
    // console.log(usrCmd)
    switch (usrCmd) {
        case 'create_parking_lot':
            try {
                const result    =  parkingLot.createLot(input)
                console.log(result)
            } catch (error) {
                console.log(chalk.red.bold(error.message))
            }
            break;
        case 'park':
            try {
                const result   =  parkingLot.parkTheCar(input)
                console.log(result)
            } catch (error) {
                console.log(chalk.red.bold(error.message))
            }
            break;
        case 'leave':
            try {
                const result    =  parkingLot.leaveCar(input)
                console.log(result)
            } catch (error) {
                console.log(chalk.red.bold(error.message))
            }
            break;
        case 'status':
            try {
                const result    =  parkingLot.getStatusOfParking()
                console.log(result)
            } catch (error) {
                
            }
            break;
        case 'registration_numbers_for_cars_with_colour':
            try {
                const result    =  parkingLot.findCarBySimilarColor(input)
                console.log(result)
            } catch (error) {
                console.log(chalk.red.bold(error.message))
            }
            break;
        case 'slot_numbers_for_cars_with_colour':
            try {
                const result    =  parkingLot.calculateOfSimilarColor(input)
                console.log(result)
            } catch (error) {
                console.log(chalk.red.bold(error.message))
            }
            break;
        case 'slot_number_for_registration_number':
            try {
                const result    =  parkingLot.findByPlate(input)
                console.log(result)
            } catch (error) {
                console.log(chalk.red.bold(error.message))
            }
            break;    
        case 'exit':
            process.exit(0)
            break;
        default:
            console.log(input, 'could not found that order!!!')
            break;
    }
    createInteractiveModeConsole()
}