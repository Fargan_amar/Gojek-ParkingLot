const Car   = require('./car')
const chalk = require('chalk')

class Parking {
    constructor () {
        this.MAXIMUM_SLOT   = 0
        this.SLOTS          = new Array()
    }

    createLot (input) {
            this.MAXIMUM_SLOT = parseInt(input.split(' ')[1])
            if (this.MAXIMUM_SLOT <= 0 || isNaN(this.MAXIMUM_SLOT)) {
                // this.MAXIMUM_SLOT   = 0
                throw new Error('Could not process, define slot first. Minimum one slot!')
            }
            for (let i = 0; i < this.MAXIMUM_SLOT; i++) {
                this.SLOTS.push(null)
            }
            return `Created a parking lot with ${this.MAXIMUM_SLOT} slots`       
    }

     checkSlot () {
        return this.SLOTS
    }

    lookingNearestSlotAvailable () {
        let status  = false
        for (let i = 0; i < this.SLOTS.length; i++) {
            if (this.SLOTS[i] == null) {
                status  = true
            }
        }
        return status
    }
    
     parkTheCar (input) {
        let  checkSlotLength = this.SLOTS.length
        
        if (this.MAXIMUM_SLOT > 0) {
            let car, carPlate, carColor
            if (this.lookingNearestSlotAvailable(this.SLOTS) == true) {
                for (let i = 0; i < checkSlotLength; i++) {
                    if (this.SLOTS[i] == null) {
                        carPlate    = input.split(' ')[1]
                        carColor    = input.split(' ')[2]
                        
                        if (carPlate && carColor) {
                            car = new Car(carPlate, carColor)
                            this.SLOTS[i]   = car
                            const num   = i+1
                            return `Allocated slot number: ${num}`
                        } else {
                            throw new Error('Plate and Color is required!')
                        }
                        
                    }
                }
            } else {
                throw new Error('Sorry, parking lot is full')
            }
        } else {
            throw new Error('Minimum one slot!!!')
        }
    }

     getStatusOfParking () {
        let arrStatus   = new Array()
        if (this.MAXIMUM_SLOT > 0) {
            arrStatus.push('Slot No. Registration No Colour')

            for (let i = 0; i < this.SLOTS.length; i++) {
                if (this.SLOTS[i] != null) {
                    let num = i + 1
                    arrStatus.push(num + '.' + '\t '+this.SLOTS[i].PLATE+ '\t ' + this.SLOTS[i].COLOR)
                }
                
            }
            if (arrStatus.length > 0) {
                return arrStatus.join('\n')
            } else {
                return 'Sorry, parking lot is empty'
            }
            
        } else {
            throw new Error('Sorry, pakring lot is empty')
        }
    }

     leaveCar(input) {
        input  = parseInt(input.split(' ')[1] - 1)
        const index = input+1
        if (this.MAXIMUM_SLOT === 0) {
            throw new Error('Parking lot is empty')
        } else {
            if (input >= this.MAXIMUM_SLOT) {
                throw new Error(`Slot  ${index} not found`)
            } else if (this.SLOTS[input] === null) {
                throw new Error(`Slot ${index} is already free `)
            } else if (input <= this.SLOTS.length && input > -1) {
                this.SLOTS[input]   = null
                return `Slot number ${index} is free`
            }
        }
    }

     findCarBySimilarColor (color) {
        color   = color.split(' ')[1]
        if (this.MAXIMUM_SLOT > 0) {
            let arrResult = []
            this.SLOTS.forEach((row, index) => {
                if (this.SLOTS[index] && row.COLOR.toLowerCase() === color.toLowerCase()) {
                    arrResult.push(row.PLATE)
                }
            });
            if (arrResult.length > 0) {
                return arrResult.join(', ')
            } else {
                return chalk.red.yellow('Not Found')
            }
        } else {
            throw new Error('Could not process, define slot first. Minimum one slot!')
        }
    }

     calculateOfSimilarColor (color) {
        color = color.split(' ')[1]
        if (this.MAXIMUM_SLOT > 0) {
            let arrResult = []
            this.SLOTS.forEach((row, index) => {
                if (this.SLOTS[index] && row.COLOR.toLowerCase() === color.toLowerCase()) {
                    arrResult.push(index +=1)
                }
            });
            if (arrResult.length > 0) {
                return arrResult.join(', ')
            } else {
                return chalk.red.yellow('Not Found')
            }
        } else {
            throw new Error('Could not process, define slot first. Minimum one slot!')
        }
    }

     findByPlate (plate) {
        plate   = plate.split(' ')[1]
        if (this.MAXIMUM_SLOT > 0) {
            let arrResult   = []
            this.SLOTS.forEach((row, index) => {
                if (this.SLOTS[index] 
                    && row.PLATE === plate) {
                    arrResult.push(index += 1)
                }
            })
            if (arrResult.length > 0) {
                return arrResult.join(', ')
            } else {
                return 'Not found'
            }
        }
    }
}

module.exports  = Parking