class Car {
    constructor(PLATE, COLOR) {
        this.PLATE  = PLATE
        this.COLOR  = COLOR
    }
 
    ifCarIsEqual (car1, car2) {
        return ((car1.PLATE.toLowerCase() === car2.PLATE.toLowerCase()) 
        && car1.COLOR.toLowerCase() === car2.COLOR.toLowerCase())
    }
}

module.exports  = Car